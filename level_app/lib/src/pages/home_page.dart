import 'package:flutter/material.dart';
import 'package:level_app/src/pages/excersices_page.dart';
import 'package:level_app/src/pages/home_body_page.dart';
import 'package:level_app/src/pages/users_page.dart';

enum Pages {
  users,
  home,
  excersices
}

class HomePage extends StatefulWidget {

  static final routeName = 'home';


  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  Pages currentPage = Pages.home;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: _callPage(currentPage),
      bottomNavigationBar: _bottomNavigationBar(),
    );
  }

  Widget _callPage(Pages actualPage) {
    switch(currentPage) {
      case Pages.users: return UsersPage();
      case Pages.home: return HomeBodyPage();
      case Pages.excersices: return ExcersicesPage();
      default: return HomeBodyPage();
    }
  }

  _bottomNavigationBar() {
    return BottomNavigationBar(
      currentIndex: currentPage.index,
      onTap: (index) {
        setState(() {
          currentPage = Pages.values[index];
        });
      },
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.group),
          title: Text('Usuarios'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text('Inicio'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.fitness_center),
          title: Text('Ejercicios'),
        )
      ],
    );
  }
}