import 'package:flutter/material.dart';
import 'package:level_app/src/bloc/login_bloc.dart';
import 'package:level_app/src/bloc/provider.dart';
import 'package:level_app/src/pages/login_page.dart';
import 'package:level_app/src/providers/user_provider.dart';
import 'package:level_app/src/utils/utils.dart';


class RegisterPage extends StatelessWidget {

  static final routeName = 'register';

  final UserProvider userProvider = UserProvider();
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _loginBackground(context),
          _loginForm(context),
        ],
      ),
    );
  }

  Widget _loginBackground(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final background = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: <Color> [
            Color.fromRGBO(63, 63, 156, 1.0),
            Color.fromRGBO(90, 70, 178, 1.0),
          ]
        )
      ),
    );
    final circle = Container(
      height: size.height * 0.15,
      width: size.height * 0.15,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        color: Color.fromRGBO(255, 255, 255, 0.05),        
      ),
    );
    final name = Container(
      padding: EdgeInsets.only(top: size.height * 0.05),
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          Icon(Icons.person_pin_circle, color: Colors.white, size: size.height * 0.15,),
          SizedBox(height: size.height * 0.02,),
          Text("Level App", style: TextStyle(color: Colors.white, fontSize: size.height * 0.03)),
        ],
      ),
    );
    return Stack(
      children: <Widget>[
        background,
        Positioned(child: circle, top: size.height * 0.1, left: size.width * 0.05),
        Positioned(child: circle, top: size.height * 0.3, left: size.width * 0.8),
        Positioned(child: circle, top: size.height * 0.01, left: size.width * 0.5),
        Positioned(child: circle, top: size.height * 0.35, left: size.width * 0.2),
        name,
      ],
    );
  }

  Widget _loginForm(BuildContext context) {
    final bloc = Provider.of(context);
    
    final size = MediaQuery.of(context).size;
    return Container(
      alignment: Alignment.topCenter,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SafeArea(
              child: Container(
                height: 160.0,
              ),
            ),
            Container(
              alignment: Alignment.center,
              width: size.width * 0.85,
              margin: EdgeInsets.symmetric(vertical: 10.0),
              padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: <BoxShadow> [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 3.0,
                    offset: Offset(0.0, 5.0),
                    spreadRadius: 3.0,
                  )
                ]
              ),
              child: Column(
                children: <Widget>[
                  Text(
                    'Registro',
                    style: TextStyle(fontSize: 20.0),
                  ),
                  SizedBox( height: 40.0,),
                  _emailInput(bloc),
                  _passwordInput(bloc),
                  SizedBox( height: 10.0,),
                  _registerButton(bloc),
                ],
              ),
            ),
            FlatButton(
              child: Text('¿Ya tienes cuenta? Inicia sesión'),
              onPressed: () => Navigator.pushReplacementNamed(context, LoginPage.routeName),
            )
          ],
        ),
      ),
    );
  }

  Widget _emailInput(LoginBloc bloc) {

    return StreamBuilder(
      stream: bloc.emailStream ,
      builder: (BuildContext context, AsyncSnapshot<String> snapshot){
        return Container(
          child:  TextField(
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              icon: Icon(Icons.alternate_email, color: Colors.deepPurple),
              hintText: 'ejemplo@correo.com',
              labelText: 'Correo electrónico',
              counterText: snapshot.data,
              errorText: snapshot.error,
            ),
            onChanged: (value) {
              bloc.changeEmail(value);
            },
          ),
        );
      },
    );


  }

  Widget _passwordInput(LoginBloc bloc) {
        return StreamBuilder(
      stream: bloc.passwordStream,
      builder: (BuildContext context, AsyncSnapshot<String> snapshot){
            return Container(
      child:  TextField(   
        obscureText: true,
        decoration: InputDecoration(
          icon: Icon(Icons.lock, color: Colors.deepPurple),
          labelText: 'Contraseña',
          counterText: snapshot.data,
          errorText: snapshot.error,
        ),
        onChanged: (value) {
          bloc.changePassword(value);
        },
      ),
    );
      },
    );

  }
  
  Widget _registerButton(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.formValidPassword,
      builder: (context, snapshot) {
        return RaisedButton(
          color: Colors.deepPurple,
          textColor: Colors.white,
          onPressed: snapshot.hasData ? () => _register(bloc, context) : null,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 55.0, vertical: 10.0),
            child: Text('Registrarse'),
          ),
        );
      }
    );
  }

  _register(LoginBloc bloc, BuildContext context) async {
    final Map response = await userProvider.createUser(bloc.email, bloc.password);
    if(response['ok']) {
      Navigator.pushReplacementNamed(context, LoginPage.routeName);
    } else {
      Utils.showAlert(context, response['message']);
    }
  }
}