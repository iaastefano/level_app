import 'package:flutter/material.dart';

class Utils {
  static bool isNumeric(String value) {
    if(value.isEmpty) return false;
    final n = num.tryParse(value);
    return (n != null);
  }

  static void showAlert(BuildContext context, String message) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () => Navigator.of(context).pop(),
            )
          ],
        );
      }
    );
  }
}