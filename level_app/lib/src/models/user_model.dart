// To parse this JSON data, do
//
//     final UserModel = userModelFromJson(jsonString);

import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {

    String id;
    String name;
    String lastname;
    String email;
    String profileImage;
    String uniqueId;

    UserModel({
        this.id,
        this.name     = '',
        this.lastname = '',
        this.email    = '',
        this.profileImage = '',
    });

    factory UserModel.fromJson(Map<String, dynamic> json) => new UserModel(
        id           : json["id"],
        name         : json["name"],
        lastname     : json["lastname"],
        email        : json["email"],
        profileImage : json["profileImage"],
    );

    Map<String, dynamic> toJson() => {
        "name"         : name,
        "lastname"     : lastname,
        "email"        : email,
        "profileImage" : profileImage,
    };
}
