import 'package:flutter/material.dart';
import 'package:level_app/src/models/user_model.dart';
import 'package:level_app/src/pages/user_page.dart';
import 'package:level_app/src/providers/users_provider.dart';

class DataSearch extends SearchDelegate {

  final usersProvider = new UsersProvider();

  String seleccion = '';

  @override
  List<Widget> buildActions(BuildContext context) {
    // las acciones de nuestro appbar
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () => query = '',
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // icono a la izquierda del appbar
        return IconButton(
          icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow,
            progress: transitionAnimation,
          ),
          onPressed: () => close(context, null),
        );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Center(
      child: Container(
        child: Text(seleccion),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    
    if(query.isEmpty) return Container();

    return FutureBuilder(
      future: usersProvider.getUsers(query),
      builder: (BuildContext context, AsyncSnapshot<List<UserModel>> snapshot) {
        if(snapshot.hasData) {
          final users = snapshot.data;
          return ListView(
            children: users.map((user) {
              return ListTile(
                leading: FadeInImage(
                  image: NetworkImage(user.profileImage),
                  placeholder: AssetImage('assets/img/no-image.jpg'),
                  width: 50.0,
                  fit: BoxFit.cover,
                  fadeInDuration: Duration(milliseconds: 100),
                ),
                title: Text(user.name),
                subtitle: Text(user.lastname),
                onTap: () {
                  close(context, null);
                  user.uniqueId = '';
                  Navigator.pushNamed(context, UserPage.routeName, arguments: user);
                },
              );
            }).toList(),
          );
        }
        else {
          return Center(child: CircularProgressIndicator(),);          
        }
      },
    );

  }

}