import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:level_app/src/preferences/user_preferences.dart';

class UserProvider {

  final String _firebaseToken = 'AIzaSyBh2KQNVzYe4UvJfffmgKjoecE8-Ysy1YY';
  final _prefs = new UserPreferences();

  Future<Map<String, dynamic>> loginUser(String email, String password) async {
    final authData = {
      'email'             : email,
      'password'          : password,
      'returnSecureToken' : true
    };

    final response = await http.post(
      'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=$_firebaseToken',
      body: json.encode(authData)
    );

    Map<String, dynamic> decodedResponse = json.decode(response.body);
    
    if(decodedResponse.containsKey('idToken')) {
      _prefs.token = decodedResponse['idToken'];
      return {'ok' : true, 'token' : decodedResponse['idToken']};
    } else {
      return {'ok' : false, 'message' : decodedResponse['error']['message']};
    }
  }

  Future<Map<String, dynamic>> createUser(String email, String password) async {
    final authData = {
      'email'             : email,
      'password'          : password,
      'returnSecureToken' : true
    };

    final response = await http.post(
      'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=$_firebaseToken',
      body: json.encode(authData)
    );

    Map<String, dynamic> decodedResponse = json.decode(response.body);
    
    if(decodedResponse.containsKey('idToken')) {
      _prefs.token = decodedResponse['idToken']; 
      return {'ok' : true, 'token' : decodedResponse['idToken']};
    } else {
      return {'ok' : false, 'message' : decodedResponse['error']['message']};
    }

  }
}