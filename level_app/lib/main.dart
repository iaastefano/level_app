import 'package:flutter/material.dart';
import 'package:level_app/src/bloc/provider.dart';
import 'package:level_app/src/pages/home_page.dart';
import 'package:level_app/src/pages/login_page.dart';
import 'package:level_app/src/pages/producto_page.dart';
import 'package:level_app/src/pages/register_page.dart';
import 'package:level_app/src/preferences/user_preferences.dart';
 
void main() async {
  final prefs = new UserPreferences();
  await prefs.initPrefs();
  runApp(MyApp());  
}
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final prefs = new UserPreferences();

    return Provider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Level App',
        //TODO: Implement token authentication
        initialRoute: prefs.token != '' ? HomePage.routeName : LoginPage.routeName,
        routes: {
          LoginPage.routeName : (BuildContext context) => LoginPage(),
          HomePage.routeName : (BuildContext context) => HomePage(),
          ProductoPage.routeName: (BuildContext context) => ProductoPage(),
          RegisterPage.routeName: (BuildContext context) => RegisterPage(),
        },
      ),
    );
  }
}